# sanekeys
Sane key mapping for emacs

This Project consists only of a single Org-File that holds the complete documentation as well as the emacs-lisp code.

You can find it [here](sanekeys.org)

